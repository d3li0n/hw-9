<?php
namespace console\controllers;


use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actionIndex()
    {
        Yii::error('Database....', __METHOD__ . ':notSavedError');
        Yii::warning('Database....', __METHOD__ . ':notSavedError');
        Yii::info('Database....', __METHOD__ . ':notSavedError');
        Yii::debug('Database....', __METHOD__ . ':notSavedError');
        $this->stdout('Test' . PHP_EOL, Console::FG_RED, Console::BG_YELLOW, Console::FRAMED);
    }
}
